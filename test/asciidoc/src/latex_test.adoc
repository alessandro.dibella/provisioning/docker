= Document Title
Doc Writer <doc@example.com>
:doctype: book
:reproducible:
//:source-highlighter: coderay
:source-highlighter: rouge
:listing-caption: Listing
// Uncomment next line to set page size (default is A4)
//:pdf-page-size: Letter

A simple http://asciidoc.org[AsciiDoc] document.

== Introduction

Let's try some Mathematical Formulas:

latexmath:[$C = \alpha + \beta Y^{\gamma} + \epsilon$]

== Conclusion

That's all, folks!