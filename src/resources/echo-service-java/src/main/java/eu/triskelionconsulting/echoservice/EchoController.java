package eu.triskelionconsulting.echoservice;

import java.time.LocalDateTime;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class EchoController {

    @GetMapping("/echo/{message}")
    @ResponseBody
    public Mono<String> handle(@PathVariable String message,  ServerHttpRequest request) {
        String clientAddress = request.getRemoteAddress().getAddress().getHostAddress();
        String serverAddress = request.getLocalAddress().getAddress().getHostAddress();
        return Mono.just(LocalDateTime.now()+": "+clientAddress+" says \""+message+"\" to "+serverAddress);
    }
}
