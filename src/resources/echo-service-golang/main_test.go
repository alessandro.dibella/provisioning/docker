// main_test.go
package main_test

import (
	"echo-service"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var a main.App

func TestMain(m *testing.M) {
	a.Initialize("echo-service-test-unit")

	code := m.Run()
	os.Exit(code)
}


func TestSimpleEcho(t *testing.T) {
	req, _ := http.NewRequest("GET", "/echo/How%20are%20you%3F", nil)
	req.Header.Set("X-Test","test")
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
}


func TestForwardEcho(t *testing.T) {
	req, _ := http.NewRequest("GET", "http://127.0.0.1:8080/echo/Hello", nil)
	req.Header.Add("X-forward-to","127.0.0.1")
	req.Header.Add("X-forward-to","127.0.0.1")
	response := executeRequest(req)
	checkResponseCode(t, http.StatusInternalServerError, response.Code)
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}