package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	url2 "net/url"
	"strings"

	"github.com/gorilla/mux"
)

type App struct {
	Router      *mux.Router
	ServiceName string
}

func (a *App) Initialize(serviceName string) {
	a.ServiceName = serviceName
	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(":8080", a.Router))
}

func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/echo/{message}", a.echo).Methods("GET")
}

func buildResponseBody(a *App, message string) string {
	preposition := "from"
	if strings.Contains(message, preposition) {
		preposition = "and"
	}
	var body strings.Builder
	body.WriteString(message)
	body.WriteString(" ")
	body.WriteString(preposition)
	body.WriteString(" ")
	body.WriteString(a.ServiceName)

	return body.String()
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithPlainText(w, code, message)
}

func respondWithMessage(w http.ResponseWriter, a *App, message string) {
	respondWithPlainText(w, http.StatusOK, message)
}

func respondWithPlainText(w http.ResponseWriter, code int, payload string) {
	response := []byte(payload)

	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(code)
	w.Write(response)
}

func propagateTracingRequests(incomingRequest *http.Request, outgoingRequest *http.Request) {
	headersToPropagate := [6]string{
		// All applications should propagate x-request-id. This header is
		// included in access log statements and is used for consistent trace
		// sampling and log sampling decisions in Istio.
		"x-request-id",

		// b3 trace headers. Compatible with Zipkin, OpenCensusAgent, and
		// Stackdriver Istio configurations.
		"x-b3-traceid",
		"x-b3-spanid",
		"x-b3-parentspanid",
		"x-b3-sampled",
		"x-b3-flags",
	}

	for _, header := range headersToPropagate {
		values := incomingRequest.Header.Values(header)
		if values != nil {
			for _, value := range values {
				outgoingRequest.Header.Add(header,value)
			}
		}
	}
}

func (a *App) echo(w http.ResponseWriter, r *http.Request) {

	requestDump, err := httputil.DumpRequest(r, true)
	if err == nil {
		log.Println(string(requestDump))
	}

	vars := mux.Vars(r)
	message,err := url2.PathUnescape(vars["message"])
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	forwardTo := r.Header.Values("X-forward-to")
	body := buildResponseBody(a, message)

	if forwardTo == nil {
		respondWithMessage(w, a,"\n\n"+body+"\n\n")
	} else {
		url := url2.URL{
			Scheme: "http",
			Host:   forwardTo[0] + ":8080",
			Path:   "/echo/" + url2.PathEscape(body),
		}

		req, err := http.NewRequest(r.Method, url.String(), nil)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}

		if len(forwardTo) > 1 {
			for _, host := range forwardTo[1:] {
				req.Header.Add("X-forward-to", host)
			}
		}

		propagateTracingRequests(r,req)

		log.Println("Forwarding the request to " + url.String())

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}
		defer resp.Body.Close()

		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			respondWithError(w, http.StatusInternalServerError, err.Error())
		} else {
			respondWithPlainText(w, resp.StatusCode, string(bodyBytes))
		}
	}

}
