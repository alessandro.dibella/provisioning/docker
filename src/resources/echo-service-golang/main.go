package main

import (
	"log"
	"os"
)

func main() {
	a := App{}
	a.Initialize(os.Getenv("SERVICE_NAME"))

	log.Println("Starting echo service on port 8080")
	a.Run(":8080")
	log.Println("Echo stopped")
}