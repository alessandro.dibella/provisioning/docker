#!/usr/bin/env bash

# exit when a command fails.
set -o errexit
# exit when your script tries to use undeclared variables.
set -o nounset
# The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

if [ ! -v SERVICE_EXECUTABLE ]; then echo "variable SERVICE_EXECUTABLE is unset"; exit 1; fi

RUNTIME_OPTS="${RUNTIME_OPTS:-} --verbose"

if [ ${RUN_IN_DEBUG_MODE} = true ] ; then
    RUNTIME_OPTS="${RUNTIME_OPTS:-} --debug"
fi

if [ ${RUN_IN_PROFILING_MODE} = 'hprof' ] || [ ${RUN_IN_PROFILING_MODE} = 'jfr' ]; then
    RUNTIME_OPTS="${RUNTIME_OPTS:-} --profile ${RUN_IN_PROFILING_MODE}"
elif [ ${RUN_IN_PROFILING_MODE} = 'jmx' ]; then
    export SPRING_JMX_ENABLED="true"
    JAVA_TOOL_OPTIONS="-Dcom.sun.management.jmxremote=true"
    JAVA_TOOL_OPTIONS="${JAVA_TOOL_OPTIONS} -Dcom.sun.management.jmxremote.ssl=false"
    JAVA_TOOL_OPTIONS="${JAVA_TOOL_OPTIONS} -Dcom.sun.management.jmxremote.local.only=false"
    JAVA_TOOL_OPTIONS="${JAVA_TOOL_OPTIONS} -Dcom.sun.management.jmxremote.authenticate=false"
    JAVA_TOOL_OPTIONS="${JAVA_TOOL_OPTIONS} -Dcom.sun.management.jmxremote.port=5000"
    JAVA_TOOL_OPTIONS="${JAVA_TOOL_OPTIONS} -Dcom.sun.management.jmxremote.rmi.port=5000"
    JAVA_TOOL_OPTIONS="${JAVA_TOOL_OPTIONS} -Dcom.sun.management.jmxremote.host=0.0.0.0"
    JAVA_TOOL_OPTIONS="${JAVA_TOOL_OPTIONS} -Djava.rmi.server.hostname=0.0.0.0"

    export JAVA_TOOL_OPTIONS
fi

JAVA_OPTS="${JAVA_OPTS} ${JAVA_EXTRA_OPTS:-}"

source /usr/local/bin/run-with-jvm ${RUNTIME_OPTS:-} ${SERVICE_EXECUTABLE} ${SERVICE_ARGS} ${SERVICE_EXTRA_ARGS}