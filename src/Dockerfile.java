ARG image_tag
ARG image_registry

FROM $image_registry/alpine:latest as builder

LABEL stage=builder

RUN apk add --no-cache autoconf bash git && \
    git clone --depth 1 https://github.com/matejak/argbash.git /usr/src/argbash

WORKDIR /usr/src/argbash/resources/

RUN apk add --no-cache --virtual .build-dependencies make \
	&& make install PREFIX=/usr/local \
	&& apk del .build-dependencies

COPY resources/run-with-jvm.argbash /
RUN /usr/local/bin/argbash /run-with-jvm.argbash -o /run-with-jvm


FROM aldib/base:$image_tag

ARG jdk_version

LABEL org.opencontainers.image.authors="alessandro.dibella@gmail.com"

COPY --from=builder /run-with-jvm /usr/local/bin/run-with-jvm

# We use https://aws.amazon.com/corretto/faqs/ because it includes performance or stability patches by Amazon that are not yet integrated in the corresponding OpenJDK update projects.
# https://stackoverflow.com/a/55829250/5243501
RUN curl --fail-with-body -kL https://apk.corretto.aws/amazoncorretto.rsa.pub -o /etc/apk/keys/amazoncorretto.rsa.pub  && \
    echo "https://apk.corretto.aws/" >> /etc/apk/repositories   && \
    apk update  && \
    apk add --no-cache java-cacerts $jdk_version ttf-dejavu  && \
    chmod +x /usr/local/bin/run-with-jvm

ENV JAVA_HOME=/usr/lib/jvm/default-jvm
ENV JRE_HOME=/usr/lib/jvm/default-jvm
ENV JAVA_OPTS=-XX:+UseContainerSupport

# https://github.com/anapsix/docker-alpine-java/issues/27
RUN sed -i 's/ca-anchors/certificates/g' /etc/ca-certificates/update.d/java-cacerts