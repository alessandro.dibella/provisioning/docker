ARG image_tag
ARG image_registry

FROM $image_registry/maven:3-openjdk-17 AS builder

LABEL stage=builder

WORKDIR /tmp

COPY resources/echo-service-java /tmp/echo-service

RUN cd /tmp/echo-service && \
    mvn --quiet --fail-fast --batch-mode clean package 
    
FROM aldib/springboot-service:$image_tag

ENV SERVICE_EXECUTABLE="/opt/srv/libs/echo-service.jar"

COPY --from=builder --chown=1000:0 /tmp/echo-service/target/echo-service-0.0.1-SNAPSHOT.jar $SERVICE_EXECUTABLE

USER root

RUN apk --no-cache add sudo bash-completion vim curl httpie jq && \    
    echo 'srv_runner        ALL=(ALL)       NOPASSWD: ALL' > /etc/sudoers.d/srv_runner && \
    chmod 0440 /etc/sudoers.d/srv_runner

USER srv_runner