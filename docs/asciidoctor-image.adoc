[[asciidoctor-oci-image]]
=== aldib/asciidoctor

This image is derived from link:https://asciidoctor.org/[Asciidoctor], and it is used convert link:https://asciidoc.org/[AsciiDoc] into link:https://en.wikipedia.org/wiki/PDF[PDF], link:https://en.wikipedia.org/wiki/HTML[HTML], link:https://en.wikipedia.org/wiki/DocBook[DocBook], link:https://en.wikipedia.org/wiki/Office_Open_XML[Office Open XML], link:https://en.wikipedia.org/wiki/Markdown[Markdown], or link:https://en.wikipedia.org/wiki/OpenDocument[OpenDocument] format. 


It includes the following additional plugins and components:

. link:https://github.com/riboseinc/asciidoctor-bibliography[asciidoctor-bibliography]
. link:https://github.com/asciidoctor/asciidoctor-confluence[asciidoctor-confluence]
. link:https://fonts.google.com/[Google fonts]
. link:https://sourceforge.net/projects/mscorefonts/[Microsoft's Core fonts for the Web]
. Support for link:https://plantuml.com/[PlantUML]

