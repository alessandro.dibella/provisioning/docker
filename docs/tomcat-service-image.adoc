[[tomcat-service-oci-image]]
=== aldib/tomcat-service

This image is derived from <<openjdk17-oci-image,aldib/openjdk17>>, and it is used as a base image for the deployment of applications packaged as link:https://en.wikipedia.org/wiki/WAR_(file_format)[Web Application Resources (WAR)].

It includes link:https://tomcat.apache.org/[Apache Tomcat] with link:https://tomcat.apache.org/native-doc/[Native Library] support.

All logs are redirected to the console, and all default applications (e.g., `/manager/`) have been removed.

Applications are expected to be deployed in `/opt/tomcat/webapps/`.

Derived images can optionally provide the following:

. Set the `JAVA_EXTRA_OPTS` environment variable with additional options to be passed to the JVM.
. Set the `RUN_IN_DEBUG_MODE=true` environment variable to run the application with JDWP enabled for remote debugging on port 9999.
. Set the `RUN_IN_PROFILING_MODE=jmx` environment variable to create link:https://docs.oracle.com/javacomponents/jmc-5-5/jfr-runtime-guide/about.htm#JFRRT107[Java Flight Recorder (JFR)] files under `/opt/srv/logs/`.

